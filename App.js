import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Router from './src/Router';

import store from './src/store';

import { Provider } from 'mobx-react'

//Navigation
import NavigationService from './src/NavigationService';


export default class App extends Component {
  render() {
    return (
      <Provider {...store}>
        <Router
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

