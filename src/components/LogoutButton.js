import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { inject } from 'mobx-react';

@inject('AuthStore')
export default class LogoutButton extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.buttonContainer}
                onPress={() => this.props.AuthStore.removeToken()}
            >
                <Text style={styles.text}>Logout</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        padding: 10
    },
    text: {
        fontSize: 14
    }
});