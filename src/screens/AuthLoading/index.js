import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { inject } from 'mobx-react';

@inject('AuthStore')
export default class AuthLoading extends Component {

    async componentDidMount() {
        await this.props.AuthStore.setupAuth();
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Loading...</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});